﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Collectible : MonoBehaviour
{
    public GameObject uWin;

    private void Start()
    {
        uWin.SetActive(false);
    }
    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            print("Collide");
            Destroy(gameObject);
            uWin.SetActive(true);
        }
    }
    
}
