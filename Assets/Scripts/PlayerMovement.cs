﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class PlayerMovement : MonoBehaviour
{
    [HideInInspector]
    public CharacterController characterContoller;
    public Animator animator;

    public float walkSpeed = 1.5f;
    public float runSpeed = 3.0f;
    public float rotationSpeed = 50.0f;
    public float gravity = -30.0f;

    [Tooltip("Only useful with Follow and Independent Rotation - third - person camera control")]
    public bool followCameraForward = false;
    public float turnRate = 200.0f;
    private Vector3 velocity = new Vector3(0.0f, 0.0f, 0.0f);

    void Start()
    {
        characterContoller = GetComponent<CharacterController>();
    }
    void Update()
    {
        Move();
    }
    public void Move()
    {
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");
        float speed = walkSpeed;

        if (Input.GetKey(KeyCode.LeftShift))
        {
            speed = runSpeed;
        }

        if (followCameraForward)
        {
            if (v > 0.1 || v < -0.1 || h > 0.1 || h < -0.1)
            {
                Vector3 eu = Camera.main.transform.rotation.eulerAngles;
                transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(0.0f, eu.y, 0.0f), turnRate * Time.deltaTime);
            }
        }
        
        else
        {
            transform.Rotate(0.0f, h * rotationSpeed * Time.deltaTime, 0.0f);
        }
        
        characterContoller.Move(transform.forward * v * speed * Time.deltaTime);

        if (animator != null)
        {
            animator.SetFloat("PosZ", v * speed / runSpeed);
        }

        velocity.y += gravity * Time.deltaTime;
        characterContoller.Move(velocity * Time.deltaTime);
        if (characterContoller.isGrounded && velocity.y < 0)
        velocity.y = 0f;
    }
}
